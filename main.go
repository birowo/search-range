package main

import (
	"fmt"
)

func findLoHi(nums []int, target float64) (int, int) {
	lo, hi := 0, len(nums)-1
	for lo <= hi {
		mi := (hi + lo) >> 1
		if float64(nums[mi]) < target {
			lo = mi + 1
		} else {
			hi = mi - 1
		}
	}
	return lo, hi
}
func searchRange(nums []int, target int) []int {
	_, hi := findLoHi(nums, float64(target)+.1)
	if hi == -1 {
		return []int{-1, -1}
	}
	lo, _ := findLoHi(nums[:hi], float64(target)-.1)
	if lo <= hi && nums[lo] == target {
		return []int{lo, hi}
	}
	return []int{-1, -1}
}
func main() {
	nums := []int{5, 7, 7, 8, 8, 10}
	rslt := searchRange(nums, 8)
	fmt.Println(rslt)
	nums = []int{1, 3, 4}
	rslt = searchRange(nums, 3)
	fmt.Println(rslt)
}
